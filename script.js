// TEST VARS:
if (typeof lightdm === 'undefined')
    lightdm = {
        hide_users: false,
        has_guest_account: false,
        hostname: 'based',
        users: [
            {name:'erik',display_name:'Erik'},
            {name:'nicolas',display_name:'Nicolas', image:'archlinux-logo.svg', session:'bspwm', logged_in:true},
            // {name:'bobby',display_name:'Bob'}
        ],
        select_user: null,
        sessions: [{name:'bspwm', key:'bspwm', comment:'Great WM'}, {name:'dwm', key:'dwm', comment:'So light-weight it flies'}],
        authenticate: ()=>{},
        layouts: ['sv'],
        languages: [],
        can_suspend: true,
        can_hibernate: true,
        can_restart: true,
        can_shutdown: true,
    };
if (typeof greeterutil === 'undefined')
    greeterutil = {
        dirlist: () => ['background.jpg','273fe93680830935468013345280f6fd-d4o4mt1.jpg', '1514026124617.jpg',
'1514805898906.jpg',
        '1516409223433.jpg',
        ],
    };
if (typeof greeter_config === 'undefined')
    greeter_config = {
        branding: {
            logo:'/usr/share/pixmaps/archlinux-logo.svg',
            background_images:'/usr/share/backgrounds',
        }
    };




const out = document.getElementById('output');

// let printobj = JSON.parse(JSON.stringify(lightdm));
// delete printobj.layouts;

// out.innerText = [
//     'Height: ' + innerHeight,
//     'Width: ' + innerWidth,
//     'Lightdm: ' + JSON.stringify(printobj),
// ].join('\n');

// const debug = document.createElement('div');
// document.body.appendChild(debug);
// setInterval(() => {
//     debug.innerText = `busy:${busy}`;
// }, 250);

const id = (id) => document.getElementById(id);
const q = (sel) => document.querySelector(sel);
const cr = (el, obj={}) => Object.assign(document.createElement(el), obj);

const time = q('.clock .time');
const date = q('.clock .date');
function updateClock() {
    const d = new Date();
    time.innerText = `${d.toLocaleTimeString('sv')}`;
    date.innerText = `${d.toLocaleDateString('sv')}`;
} updateClock();
setInterval(updateClock, 1000);


// ==========================
// REQUIRED GREETER FUNCTIONS
// ==========================
window.show_prompt = (text, type) => {
    // out.innerText += `\nshow_prompt called: ${text}, ${type}`;
    if (type === 'password')
        lightdm.respond(id('password').value);
};

window.show_message = (text, type) => {
    // out.innerText += `\nshow_message called: ${text}, ${type}`;
};

window.authentication_complete = () => {
    onSuccess();
};

window.autologin_timer_expired = () => {
    // out.innerText += '\nautologin_timer_expired called';
};


//
//
//

let users = {}; // username:Object
let busy = false;

// Init
(() => {
    let selected = 0;
    if (lightdm.has_guest_account) {
        users['Guest'] = { name:'Guest' };
        if (!lightdm.select_guest)
            selected++;
    }
    if (!lightdm.hide_users)
        lightdm.users.forEach(user => users[user.name] = user);
    
    const ul = cr('ul');
    Object.values(users).forEach((user, i) => {
        user.el = cr('li', { username:user.name, innerText:user.display_name || user.name });
        user.el.appendChild(cr('span', { className:'hostname', innerText:lightdm.hostname }));
        ul.appendChild(user.el);
        if (user.logged_in)
            selected = i;
    });
    id('username').appendChild(ul);

    id('session').innerHTML = lightdm.sessions.map(ses => `<option value=${ses.key}>${ses.name} - ${ses.comment}</option>`).join('\n');
    
    selectUser(lightdm.select_user || Object.keys(users)[Math.min(selected, )]);

    setTimeout(() => id('password').focus(), 100);
    // id('password').addEventListener('blur', ev => {
    //     if (!busy)
    //         ev.target.focus();
    // });

    if (!lightdm.can_suspend) id('suspend').style.display = 'none';
    if (!lightdm.can_hibernate) id('hibernate').style.display = 'none';
    if (!lightdm.can_restart) id('restart').style.display = 'none';
    if (!lightdm.can_shutdown) id('shutdown').style.display = 'none';

    if (lightdm.layouts.length < 2) id('layout').classList.add('disabled');
})();

function selectUser(username) {
    if (!username) return;
    if (busy) return;
    q('.selected')?.classList.remove('selected');
    const user = users[username];
    user.el.classList.add('selected');
    if (user.session)
        id('session').value = user.session;
    if (user.logged_in) {
        id('session').style.display = 'none';
        q('.loggedIn').style.display = 'block';
        id('currentSession').innerText = user.session;
        id('login').value = 'Unlock';
    } else {
        q('.loggedIn').style.display = 'none';
        id('session').style.display = 'inline';
        id('login').value = 'Login';
    }
}

let clearWarningId;
function tryLogin() {
    if (busy) return;
    busy = true;
    clearWarning();
    q('.center').classList.add('disabled');
    q('.tryLogin').style.display = 'block';
    lightdm.authenticate(q('.selected').username);
    const id = setInterval(() => {
        if (!lightdm.in_authentication)
            if (!lightdm.is_authenticated) {
                clearInterval(id);
                onFailed();
            }
    }, 100);
}

function onFailed() {
    busy = false;
    id('password').focus();
    q('.center').classList.remove('disabled');
    const tl = q('.tryLogin');
    tl.style.color = 'var(--color5)';
    tl.innerText = 'Wrong password!';
    clearWarningId = setTimeout(clearTimeout, 5000);
}

function onSuccess() {
    lightdm.start_session_sync(id('session').value);
}

function clearWarning() {
    if (!clearWarningId) return;
    clearTimeout(clearWarningId);
    const tl = q('.tryLogin');
    tl.style.color = 'var(--color4)';
    tl.style.display = 'none';
    tl.innerText = 'Checking password';
    clearWarningId = null;
}

function openPrompt(text, btns) { // {innerText:String, className:String, onclick:function}
    id('prompt').innerText = text;
    const elBtns = id('promptBtns');
    elBtns.innerHTML = '';
    btns.forEach(({innerText, className='', onclick}) => {
        const el = cr('button', { innerText, className, onclick });
        elBtns.appendChild(el);
    });
    q('.overlay').style.display = 'block';
}

function closePrompt() {
    q('.overlay').style.display = 'none';
    id('promptBtns').innerHTML = '';
}

id('username').addEventListener('click', ev => {
    const username = ev.target.username;
    if (!username) return;
    if (ev.target.classList.contains('selected')) return;
    selectUser(username);
});

q('input[type=submit]').addEventListener('click', ev => {
    tryLogin();
});

id('suspend').addEventListener('click', () => lightdm.suspend()); // sleep
id('hibernate').addEventListener('click', () => lightdm.hibernate()); // turn off but save session
id('restart').addEventListener('click', () => lightdm.restart());
id('shutdown').addEventListener('click', () => openPrompt('Are you sure that you want to shutdown?', [
    { innerText:'Yes', onclick:()=>lightdm.shutdown() }, { innerText:'No', onclick:closePrompt }]));

document.addEventListener('keydown', ev => {
    if (busy)
        return false;
    clearWarning();
    switch (ev.key) {
        case 'ArrowUp': selectUser(q('.selected')?.previousElementSibling?.username); break;
        case 'ArrowDown': selectUser(q('.selected')?.nextElementSibling?.username); break;
        case 'Enter': if (document.activeElement.id === 'password') tryLogin(); break;
        case 'Escape': Settings.close(); break;
        // default: id('password').focus();
    }
});







// ==========
// BACKGROUND
// ==========
const Background = {
    currentCss: null,
    currentSrc: null,
    srcList: null,
    init: function() {
        this.setSrc(localStorage.getItem('background-src'));
        this.setCSS(localStorage.getItem('background-css'));
    },
    getSrcList: function() {
        if (this.srcList)
            return this.srcList;
        this.srcList = [];
        const searchDir = dir => {
            try {
                greeterutil.dirlist(dir).forEach(path => {
                    if (/\.(png|jpeg|jpg|gif|svg)$/.test(path)) {
                        this.srcList.push(path);
                    } else if (!/\./.test(path)) {
                        searchDir(path);
                    }
                });
            } catch (e) {
                // Dir path is probably not a dir, ie unknown file format/not image file
            }
        };
        searchDir(greeter_config.branding.background_images);
        return this.srcList;
    },
    setCSS: function(css) {
        if (css == null)
            css = '';
        this.currentCss = css;
        id('background').setAttribute('style', css);
        localStorage.setItem('background-css', css);
    },
    setSrc: function(src) {
        if (src == null)
            src = '';
        this.currentSrc = src;
        id('background').src = src;
        localStorage.setItem('background-src', src);
    },
};




// ========
// SETTINGS
// ========
const Settings = {
    element: null,
    created: false,
    init: function() {
        id('openSettings').addEventListener('click', () => this.open());
        this.element = q('.settings');
        this.element.addEventListener('click', ev => {
            if (ev.target === this.element)
                this.close();
        });
        id('backgroundCss').addEventListener('change', ev => {
            Background.setCSS(ev.target.value);
        });
    },
    create: function() {
        const canvas = cr('canvas', { width:190, height:Math.round(190*innerHeight/innerWidth) });
        const context = canvas.getContext('2d');
        const backgrounds = q('.backgrounds');
        id('backgroundCss').value = Background.currentCss;
        id('backgroundsFound').innerText = `(Found ${Background.getSrcList().length})`;
        let processQueue = [];
        let busy = false;
        const nextInQueue = () => {
            if (!busy && processQueue.length) {
                busy = true;
                setTimeout(() => {
                    processQueue.shift()();
                    busy = false;
                    nextInQueue();
                }, 20);
            }
        };
        Background.getSrcList().forEach(src => {
            let className = 'preview';
            if (Background.currentSrc === src)
                className += ' current';
            const el = cr('img', { className });
            el.addEventListener('click', () => this.onClickedImg(el, src));
            backgrounds.appendChild(el);
            const img = new Image();
            img.src = src;
            img.onload = () => {
                processQueue.push(() => {
                    context.clearRect(0, 0, canvas.width, canvas.height);
                    context.drawImage(img, 0, 0, canvas.width, canvas.height);
                    el.src = canvas.toDataURL();
                });
                nextInQueue();
            };
        });
        this.created = true;
    },
    close: function() {
        this.element.classList.remove('shown');
    },
    open: function() {
        this.element.classList.add('shown');
        if (!this.created)
            this.create();
    },
    onClickedImg: function(el, src) {
        q('.current')?.classList.remove('current');
        if (src === Background.currentSrc) {
            Background.setSrc(null);
        } else {
            Background.setSrc(src);
            el.classList.add('current');
        }
    }
};




Background.init();
Settings.init();