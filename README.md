# LightDM Webkit2 Greeter theme

A modern, dark, minimal theme for LightDM.

## Requirements

- `lightdm`
- `lightdm-webkit2-greeter`
- `light-locker` [optional - to use as lock screen]

## Setup

### Edit /etc/lightdm/lightdm.conf

Set `greeter-session=lightdm-webkit2-greeter`.

### Install theme

Move this directory to `/usr/share/lightdm-webkit/themes/<theme_name>` where `<theme_name>` can be anything, but has to be the same in the next step.

### Edit /etc/lightdm/lightdm-webkit2-greeter.conf

Set `webkit_theme = <theme_name>`.

## Screenshot

![Login screen screenshot](screenshot.png)
